# DICOM Store Services #

This is a simple demo or proof-of-concept application that implements the basic Data Intake Service workflow. It
comprises the following components:

* PostgreSQL database instance
* RabbitMQ messaging service
* XNAT DICOM Store service
* XNAT DICOM Metadata Extractor
* XNAT DICOM Aggregator

The application can be started in two different configurations:

* The **dev** configuration starts the database and messaging services in Docker containers, with the DICOM services
  launched as Java processes running in the background.
* The full or default configuration starts the database, messaging, and DICOM services in Docker containers.

Learn how to launch these configurations in the sections below. 

## Building ##

The DICOM services are all run from the same code but with different configurations based on the specified profile, or 
the default profile if you don't specify a profile at launch. The primary artifact for running these services is the
product of the **bootJar** build. To build this, run the following command:

```
$ ./gradlew bootJar
```

You can also build Docker containers for each of the components with the **composeBuild** task:

```
$ ./gradlew composeBuild
```

There is a lightweight build task that just builds the Docker containers for PostgreSQL and RabbitMQ:

```
$ ./gradlew devComposeBuild
```

## Running ##

As mentioned earlier, you can run this application in either the **dev** or full configuration,.

### Dev configuration ##

The **dev** configuration provides the infrastructure components necessary for running the various DICOM services by
building and instantiating Docker containers for PostgreSQL and RabbitMQ. You can then run one or more DICOM services
from the command line or from within your development environment.

To launch the **dev** configuration, start by creating the PostgreSQL and RabbitmQ containers:

```
$ ./gradlew devComposeUp
```
   
You can also launch these containers directly with **docker-compose**:

```
$ docker-compose --file docker/docker-compose.yml up --detach
```

The database is accessible through **localhost** on port 5432. RabbitMQ is accessible through **localhost** on port 5672
for AMQP messages and port 15672 for the web interface. The username and password for both services are **xnat** and 
**xnat**.  
  
Once you have your infrastructure services running, you can launch one or more of the DICOM services. To easily launch
all of them, run the **dev-launch.sh** script:
 
```
$ ./dev-launch.sh
``` 

You can stop these services with the **dev-kill.sh** script:

```
$ ./dev-kill.sh
```

The launch script basically runs the following command for each service (it also includes options to allow connecting to
the process with a debugger, but this is omitted here for clarity):

```
$ java -jar -Dspring.profiles.active=dev,<PROFILE> build/libs/dicom-store-1.0-SNAPSHOT-boot.jar --mode=<PROFILE> ./store
```

When **PROFILE** is **capture**, it also adds the options **--ae-title=XNAT --port=8104**.

### Full configuration ###

The full configuration runs all the services, each in its own Docker container. The easiest way to run the full
configuration is using the **composeUp** Gradle task:

```
$ ./gradlew composeUp
```

You can also start the full configuration directly with **docker-compose**:

```
$ docker-compose --file docker-compose.yml --file docker-compose-services.yml up --detach
```

When running directly with **docker-compose**, make sure you've built the Boot jar as well as the Docker images for the 
different service configurations. **docker-compose** won't verify whether the images use the latest code or rebuild the
application for you.

## Stopping ##

### Dev configuration ###

To stop containers in the **dev** configuration, you should stop the DICOM service process or processes first. There are
a few different ways to shut these down:

If you launched using the **dev-launch.sh** script, you can stop them with **dev-kill.sh**:

```
$ ./dev-kill.sh
```

You can stop a particular process by POSTing to the shutdown endpoint provided by the application:

```
$ curl --user xnat:xnat --request POST http://localhost:8080/actuator/shutdown
```

Note that you'll need to change the port depending on which service you're trying to shut down:

| Service   | Port |
| --------- | ---- |
| Capture   | 8080 |
| Extract   | 8081 |
| Aggregate | 8082 |

### Full configuration ###

To shut down the full configuration, you should use the same method with which you started it:

* If you started with Gradle, use the **composeDown** task:

  ```
  $ ./gradlew composeDown
  ```

* If you started with **docker-compose**, use that:

  ```
  $ docker-compose --file docker-compose.yml --file docker-compose-services.yml up --detach
  ```

## Cleaning up ##

You may need to reset your configuration to get rid of old database entries, stored DICOM data, etc. You need to remove
the following items to fully reset:

* The application **build** folder. You can use one of the following commands:

  ```
  $ rm -rf build
  $ ./gradlew clean
  ```

* The container images:

  ```
  $ docker rmi xdcm-capture xdcm-aggregate xdcm-extract xdcm-db xdcm-queue
  ```

* The database's persistent volume:

  ```
  $ docker volume rm xdcm-db-data
  ```

* The **store** folder:

  ```
  $ rm -rf store
  ```
