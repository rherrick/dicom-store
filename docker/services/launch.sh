#!/bin/bash

STARTUP_OPTIONS=()
[[ -n ${CAPTURE_AE_TITLE} ]] && { STARTUP_OPTIONS+=("--ae-title=${CAPTURE_AE_TITLE}"); }
[[ -n ${CAPTURE_AE_PORT} ]] && { STARTUP_OPTIONS+=("--port=${CAPTURE_AE_PORT}"); }
[[ -n ${CAPTURE_AE_FOLDER} ]] && { STARTUP_OPTIONS+=("${CAPTURE_AE_FOLDER}"); }
CMD_LINE_OPTIONS=$(IFS=$' '; echo "${STARTUP_OPTIONS[@]}")

echo "CAPTURE_AE_TITLE: ${CAPTURE_AE_TITLE}"
echo "CAPTURE_AE_PORT: ${CAPTURE_AE_PORT}"
echo "CAPTURE_AE_FOLDER: ${CAPTURE_AE_FOLDER}"
echo "Starting application with command line: java -jar -Dspring.profiles.active=${STARTUP_PROFILE} /opt/dicom/app.jar ${CMD_LINE_OPTIONS}"
java -jar -Dspring.profiles.active=${STARTUP_PROFILE} /opt/dicom/app.jar ${CMD_LINE_OPTIONS}
