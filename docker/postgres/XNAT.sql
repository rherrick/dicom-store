/*
 * dicom-store: /Users/rherrick/Development/XNAT/Tools/Clients/DICOM/dicom-store/docker/postgres/XNAT.sql
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

CREATE USER xnat NOCREATEDB;
CREATE DATABASE xnat OWNER xnat;
ALTER USER xnat WITH PASSWORD 'xnat';
