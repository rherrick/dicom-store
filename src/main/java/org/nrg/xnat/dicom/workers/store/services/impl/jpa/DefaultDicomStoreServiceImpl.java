/*
 * dicom-store: org.nrg.xnat.dicom.workers.store.services.impl.jpa.DefaultDicomStoreServiceImpl
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.dicom.workers.store.services.impl.jpa;

import static org.nrg.xnat.dicom.workers.store.services.CaptureRecordService.CAPTURE;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.StopWatch;
import org.dcm4che3.data.Attributes;
import org.dcm4che3.data.Tag;
import org.dcm4che3.data.VR;
import org.dcm4che3.io.DicomInputStream;
import org.dcm4che3.io.DicomOutputStream;
import org.dcm4che3.net.*;
import org.dcm4che3.net.pdu.PresentationContext;
import org.dcm4che3.net.service.BasicCEchoSCP;
import org.dcm4che3.net.service.BasicCStoreSCP;
import org.dcm4che3.net.service.DicomServiceException;
import org.dcm4che3.net.service.DicomServiceRegistry;
import org.nrg.xnat.dicom.workers.store.LaunchCommand;
import org.nrg.xnat.dicom.workers.store.data.CaptureRecord;
import org.nrg.xnat.dicom.workers.store.services.CaptureRecordService;
import org.nrg.xnat.dicom.workers.store.services.DicomStoreService;
import org.nrg.xnat.dicom.workers.store.utilities.DataManager;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.converter.json.Jackson2ObjectMapperFactoryBean;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.GeneralSecurityException;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Profile(CAPTURE)
@Service
@Getter(AccessLevel.PROTECTED)
@Accessors(prefix = "_")
@Slf4j
public class DefaultDicomStoreServiceImpl extends BasicCStoreSCP implements DicomStoreService {
    @Autowired
    public DefaultDicomStoreServiceImpl(final CaptureRecordService captureRecordService, final RabbitTemplate rabbitTemplate, final Jackson2ObjectMapperFactoryBean objectMapperBuilderFactory, final LaunchCommand command, final ExecutorService executorService, final ScheduledExecutorService scheduledExecutorService) throws IOException {
        super("*");

        _status = 0;
        _aeTitle = command.getAeTitle();
        _port = command.getPort();
        _storageFolder = DataManager.validateFolder(command.getDestination());
        _downloadFolder = DataManager.validateFolder(_storageFolder.resolve("downloads"));
        _captureRecordService = captureRecordService;
        _rabbitTemplate = rabbitTemplate;
        _objectMapper = Objects.requireNonNull(objectMapperBuilderFactory.getObject());
        _receiveDelays = ZERO_ARRAY;
        _responseDelays = ZERO_ARRAY;
        _connection = initConnection(_port);
        _applicationEntity = initApplicationEntity(_connection, _aeTitle);

        _device = new Device(command.getServiceName());
        _device.setDimseRQHandler(createServiceRegistry());
        _device.addConnection(_connection);
        _device.addApplicationEntity(_applicationEntity);
        _device.setExecutor(executorService);
        _device.setScheduledExecutor(scheduledExecutorService);
        try {
            _device.bindConnections();
        } catch (GeneralSecurityException e) {
            throw new RuntimeException("An error occurred", e);
        }

        _processor = _objectMapper.createObjectNode();
        _processor.put("implementation", getClass().getName());

        log.info("Completed create DICOM store service for AE {}:{}, storage folder set to {}", _aeTitle, _port, _storageFolder);
    }

    @Override
    protected void store(final Association incoming, final PresentationContext context, final Attributes request, final PDVInputStream data, final Attributes response) throws DicomServiceException {
        if (response == null) {
            throw new RuntimeException("The response object is null");
        }

        final CaptureRecord captureRecord = capture(incoming, context, request, data, response);
        // TODO: Explicit references to RabbitMQ should be replaced by message service that can send to the appropriate destination.
        _rabbitTemplate.convertAndSend(CAPTURE, captureRecord);
    }

    @Override
    public CaptureRecord capture(final Association incoming, final PresentationContext context, final Attributes request, final PDVInputStream data, final Attributes response) throws DicomServiceException {
        final StopWatch stopWatch = StopWatch.createStarted();
        sleep(incoming, getReceiveDelays());

        try {
            response.setInt(Tag.Status, VR.US, getStatus());

            final String affectedSOPClassUID    = request.getString(Tag.AffectedSOPClassUID);
            final String affectedSOPInstanceUID = request.getString(Tag.AffectedSOPInstanceUID);
            final String transferSyntaxUID      = context.getTransferSyntax();

            final Path downloadFolder;
            try {
                downloadFolder = DataManager.validateFolder(Files.createTempDirectory(getDownloadFolder(), "download-"));
            } catch (IOException e) {
                throw new DicomServiceException(Status.ProcessingFailure, new Exception("An error occurred trying to create a temporary directory under the download folder " + getDownloadFolder(), e));
            }

            final File downloadFile = downloadFolder.resolve(affectedSOPClassUID + PART_EXT).toFile();
            try {
                storeTo(incoming, incoming.createFileMetaInformation(affectedSOPInstanceUID, affectedSOPClassUID, transferSyntaxUID), data, downloadFile);
            } catch (IOException e) {
                throw new DicomServiceException(Status.ProcessingFailure, new Exception("An error occurred trying to store incoming data to the file " + downloadFile.getAbsolutePath(), e));
            }

            final File destinationFile = getDestinationFile(downloadFile);
            try {
                renameTo(incoming, downloadFile, destinationFile);
            } catch (Exception e) {
                deleteFile(incoming, destinationFile);
                throw new DicomServiceException(Status.ProcessingFailure, new Exception("An error occurred trying to move an incoming DICOM file from " + downloadFile.getAbsolutePath() + " to " + destinationFile.getAbsolutePath() + ". Leaving download file for forensics purposes.", e));
            }
            try {
                if (downloadFolder.toFile().exists()) {
                    FileSystemUtils.deleteRecursively(downloadFolder);
                }
            } catch (IOException e) {
                log.warn("Successfully received DICOM data and moved to file {}, but something went wrong trying to delete the temporary download folder.", downloadFolder, e);
            }
            stopWatch.stop();
            log.info("Completed saving {} bytes of DICOM to file {} in {} ms", DataManager.getFileSize(destinationFile), destinationFile, stopWatch.getTime(TimeUnit.MILLISECONDS));

            final ObjectNode metadata = getMetadata(destinationFile);
            metadata.put("transferSyntax", context.getTransferSyntax());

            return _captureRecordService.create(CaptureRecord.builder()
                                                             .format("DICOM")
                                                             .location(destinationFile.toURI().toString())
                                                             .metadata(metadata)
                                                             .processor(getProcessorInfo())
                                                             .captureStartTime(new Date(stopWatch.getStartTime()))
                                                             .captureEndTime(new Date())
                                                             .build());
        } finally {
            sleep(incoming, getResponseDelays());
            if (stopWatch.isStarted()) {
                log.warn("The stop watch wasn't stopped, so something probably went wrong earlier. The total time taken to this point is {} ms", stopWatch.getTime(TimeUnit.MILLISECONDS));
            }
        }
    }

    private JsonNode getProcessorInfo() {
        return null;
    }

    private ObjectNode getMetadata(final File file) {
        final ObjectNode node = _objectMapper.createObjectNode();
        node.put("name", file.getName());
        node.put("uri", file.toURI().toString());
        node.put("size", file.length());
        node.put("path", file.getAbsolutePath());
        node.put("updated", file.lastModified());
        return node;
    }

    private void sleep(final Association association, final int[] delays) {
        final int responseDelay = delays != null ? delays[(association.getNumberOfReceived(Dimse.C_STORE_RQ) - 1) % delays.length] : 0;
        if (responseDelay > 0) {
            try {
                Thread.sleep(responseDelay);
            } catch (InterruptedException ignore) {
            }
        }
    }

    private void storeTo(final Association association, final Attributes fileMetaInformation, final PDVInputStream data, final File file) throws IOException {
        log.debug("{}: M-WRITE {}", association, file);
        file.getParentFile().mkdirs();
        try (final DicomOutputStream out = new DicomOutputStream(file)) {
            out.writeFileMetaInformation(fileMetaInformation);
            data.copyTo(out);
        }
    }

    private File getDestinationFile(final File incoming) throws DicomServiceException {
        try (final DicomInputStream input = new DicomInputStream(incoming)) {
            final Attributes attributes  = input.readDataset(-1, Tag.SeriesInstanceUID + 1);
            final Path       destination = getStorageFolder().resolve(attributes.getString(Tag.StudyInstanceUID)).resolve(attributes.getString(Tag.SeriesInstanceUID));
            destination.toFile().mkdirs();
            return destination.resolve(attributes.getString(Tag.SOPInstanceUID) + "-" + CALENDAR.getTimeInMillis() + DICOM_EXT).toFile();
        } catch (IOException e) {
            throw new DicomServiceException(Status.ProcessingFailure, new Exception("An error occurred trying to read incoming DICOM data", e));
        }
    }

    private DicomServiceRegistry createServiceRegistry() {
        final DicomServiceRegistry serviceRegistry = new DicomServiceRegistry();
        serviceRegistry.addDicomService(new BasicCEchoSCP());
        serviceRegistry.addDicomService(this);
        return serviceRegistry;
    }

    private static Connection initConnection(final int port) {
        final Connection connection = new Connection();
        connection.setPort(port);
        connection.setReceivePDULength(Connection.DEF_MAX_PDU_LENGTH);
        connection.setSendPDULength(Connection.DEF_MAX_PDU_LENGTH);
        connection.setMaxOpsInvoked(0);
        connection.setMaxOpsPerformed(0);
        connection.setPackPDV(false);
        connection.setConnectTimeout(0);
        connection.setRequestTimeout(0);
        connection.setAcceptTimeout(0);
        connection.setReleaseTimeout(0);
        connection.setSendTimeout(0);
        connection.setStoreTimeout(0);
        connection.setResponseTimeout(0);
        connection.setRetrieveTimeout(0);
        connection.setRetrieveTimeoutTotal(true);
        connection.setIdleTimeout(0);
        connection.setSocketCloseDelay(Connection.DEF_SOCKETDELAY);
        connection.setSendBufferSize(0);
        connection.setReceiveBufferSize(0);
        connection.setTcpNoDelay(false);
        return connection;
    }

    private static ApplicationEntity initApplicationEntity(final Connection connection, final String aeTitle) {
        final ApplicationEntity applicationEntity = new ApplicationEntity("*");
        applicationEntity.setAssociationAcceptor(true);
        applicationEntity.addConnection(connection);
        applicationEntity.setAETitle(aeTitle);
        applicationEntity.setAssociationAcceptor(true);
        applicationEntity.addTransferCapability(new TransferCapability(null, "*", TransferCapability.Role.SCP, "*"));
        return applicationEntity;
    }

    private static void renameTo(final Association association, final File source, final File destination) throws IOException {
        log.debug("{}: M-RENAME {} to {}", association, source, destination);
        if (!destination.getParentFile().mkdirs()) {
            destination.delete();
        }
        if (!source.renameTo(destination)) {
            throw new IOException("Failed to rename " + source + " to " + destination);
        }
    }

    private static void deleteFile(final Association association, final File file) {
        log.debug(file.delete() ? "{}: M-DELETE {}" : "{}: M-DELETE {} failed!", association, file);
    }

    private static final int[]    ZERO_ARRAY = {0};
    private static final String   PART_EXT   = ".part";
    private static final String   DICOM_EXT  = ".dcm";
    private static final Calendar CALENDAR   = Calendar.getInstance();

    @Getter(AccessLevel.PUBLIC)
    private final int    _status;
    @Getter(AccessLevel.PUBLIC)
    private final String _aeTitle;
    @Getter(AccessLevel.PUBLIC)
    private final int    _port;

    private final ObjectNode           _processor;
    private final ApplicationEntity    _applicationEntity;
    private final Connection           _connection;
    private final Path                 _storageFolder;
    private final Path                 _downloadFolder;
    private final Device               _device;
    private final CaptureRecordService _captureRecordService;
    private final RabbitTemplate       _rabbitTemplate;
    private final ObjectMapper         _objectMapper;
    private final int[]                _receiveDelays;
    private final int[]                _responseDelays;
}
