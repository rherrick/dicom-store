/*
 * dicom-store: org.nrg.xnat.dicom.workers.store.services.DicomMetadataService
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.dicom.workers.store.services;

import org.nrg.xnat.dicom.workers.store.data.CaptureRecord;
import org.nrg.xnat.dicom.workers.store.data.ResourceRecord;

public interface DicomMetadataService {
    ResourceRecord extract(final CaptureRecord captureRecord);
}
