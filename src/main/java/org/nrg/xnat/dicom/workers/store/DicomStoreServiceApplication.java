/*
 * dicom-store: org.nrg.xnat.dicom.workers.store.DicomStoreServiceApplication
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.dicom.workers.store;

import static org.nrg.xnat.dicom.workers.store.services.CaptureRecordService.CAPTURE;
import static org.nrg.xnat.dicom.workers.store.services.ResourceRecordService.EXTRACT;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Triple;
import org.springframework.amqp.core.Queue;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.ExitCodeGenerator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.autoconfigure.security.servlet.EndpointRequest;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.converter.json.Jackson2ObjectMapperFactoryBean;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.atomic.AtomicInteger;

@SpringBootApplication
@Slf4j
public class DicomStoreServiceApplication extends WebSecurityConfigurerAdapter implements ApplicationRunner, ExitCodeGenerator {
    public static void main(final String[] arguments) {
        SpringApplication.run(DicomStoreServiceApplication.class, arguments);
    }

    @Override
    public void run(final ApplicationArguments arguments) {
        if (arguments.containsOption("mode")) {
            final List<String> modes = arguments.getOptionValues("mode");
            if (modes.isEmpty()) {
                throw new RuntimeException("You must provide a mode with the --mode=<MODE> command-line parameter.");
            }
            if (modes.size() > 1) {
                throw new RuntimeException("You must provide only one mode with the --mode=<MODE> command-line parameter.");
            }
        }
        if (arguments.containsOption("ae-title")) {
            final List<String> titles = arguments.getOptionValues("ae-title");
            if (titles.isEmpty()) {
                throw new RuntimeException("You must provide an AE title with the --ae-title=<AE> command-line parameter.");
            }
            if (titles.size() > 1) {
                throw new RuntimeException("You must provide only one AE title with the --ae-title=<AE> command-line parameter.");
            }
        }
        if (arguments.containsOption("port")) {
            final List<String> ports = arguments.getOptionValues("port");
            if (ports.isEmpty()) {
                throw new RuntimeException("You must provide an port with the --port=<PORT> command-line parameter.");
            }
            if (ports.size() > 1) {
                throw new RuntimeException("You must provide only one port with the --port=<PORT> command-line parameter.");
            }
        }
    }

    @Override
    public int getExitCode() {
        return _exitCode.get();
    }

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http.csrf().disable()
            .requestMatcher(EndpointRequest.toAnyEndpoint()).authorizeRequests(requests -> requests.anyRequest().hasRole("ADMIN"))
            .httpBasic();
    }

    @Bean
    public ExecutorService executorService() {
        return Executors.newFixedThreadPool(10);
    }

    @Bean
    public ScheduledExecutorService scheduledExecutorService() {
        return Executors.newSingleThreadScheduledExecutor();
    }

    @Bean
    public Jackson2ObjectMapperFactoryBean objectMapperFactoryBean() {
        return new Jackson2ObjectMapperFactoryBean();
    }

    @Bean
    public Queue captureQueue() {
        return new Queue(CAPTURE, true);
    }

    @Bean
    public Queue extractQueue() {
        return new Queue(EXTRACT, true);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    @Override
    public UserDetailsService userDetailsService() {
        final InMemoryUserDetailsManager userDetailsManager = new InMemoryUserDetailsManager();
        USERS.stream().map(triple -> User.builder().username(triple.getLeft()).password(passwordEncoder().encode(triple.getMiddle())).roles(triple.getRight().toArray(EMPTY)).build()).forEach(userDetailsManager::createUser);
        return userDetailsManager;
    }

    private static final String[] EMPTY = new String[0];

    private static final List<Triple<String, String, List<String>>> USERS = Arrays.asList(Triple.of("xnat", "xnat", Arrays.asList("ADMIN", "USER")),
                                                                                          Triple.of("user1", "user1", Collections.singletonList("USER")),
                                                                                          Triple.of("user2", "user2", Collections.singletonList("USER")));

    private final AtomicInteger _exitCode = new AtomicInteger();
}
