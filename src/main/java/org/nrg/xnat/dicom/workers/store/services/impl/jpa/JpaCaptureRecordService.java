/*
 * dicom-store: org.nrg.xnat.dicom.workers.store.services.impl.jpa.JpaCaptureRecordService
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.dicom.workers.store.services.impl.jpa;

import javax.transaction.Transactional;
import lombok.Getter;
import org.nrg.xnat.dicom.workers.store.data.CaptureRecord;
import org.nrg.xnat.dicom.workers.store.repositories.CaptureRecordRepository;
import org.nrg.xnat.dicom.workers.store.services.CaptureRecordService;
import org.springframework.stereotype.Service;

@Service
@Getter
@Transactional
public class JpaCaptureRecordService extends AbstractRecordService<CaptureRecord, CaptureRecordRepository> implements CaptureRecordService {
    public JpaCaptureRecordService(final CaptureRecordRepository repository) {
        this.repository = repository;
    }

    private final CaptureRecordRepository repository;
}
