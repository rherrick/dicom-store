/*
 * dicom-store: org.nrg.xnat.dicom.workers.store.utilities.JacksonJsonGenerator
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.dicom.workers.store.utilities;

import com.fasterxml.jackson.core.JsonFactoryBuilder;
import java.io.IOException;
import java.io.Writer;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.json.JsonValue;
import javax.json.stream.JsonGenerator;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class JacksonJsonGenerator implements JsonGenerator {
    public JacksonJsonGenerator(final Writer writer) throws IOException {
        _generator = new JsonFactoryBuilder().build().createGenerator(writer);
    }

    @Override
    public JsonGenerator writeStartObject() {
        try {
            _generator.writeStartObject();
            return this;
        } catch (IOException e) {
            log.error("An error occurred", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public JsonGenerator writeStartObject(final String value) {
        try {
            _generator.writeObjectFieldStart(value);
            return this;
        } catch (IOException e) {
            log.error("An error occurred", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public JsonGenerator writeKey(final String s) {
        try {
            _generator.writeStartObject(s);
            return this;
        } catch (IOException e) {
            log.error("An error occurred", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public JsonGenerator writeStartArray() {
        try {
            _generator.writeStartArray();
            return this;
        } catch (IOException e) {
            log.error("An error occurred", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public JsonGenerator writeStartArray(final String value) {
        try {
            _generator.writeStartArray(value);
            return this;
        } catch (IOException e) {
            log.error("An error occurred", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public JsonGenerator write(final String s, final JsonValue jsonValue) {
        try {
            _generator.writeStartObject(s);
            _generator.writeRaw(jsonValue.toString());
            _generator.writeEndObject();
            return this;
        } catch (IOException e) {
            log.error("An error occurred", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public JsonGenerator write(final String field, final String value) {
        try {
            _generator.writeStringField(field, value);
            return this;
        } catch (IOException e) {
            log.error("An error occurred", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public JsonGenerator write(final String field, final BigInteger value) {
        try {
            _generator.writeNumberField(field, value);
            return this;
        } catch (IOException e) {
            log.error("An error occurred", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public JsonGenerator write(final String field, final BigDecimal value) {
        try {
            _generator.writeNumberField(field, value);
            return this;
        } catch (IOException e) {
            log.error("An error occurred", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public JsonGenerator write(final String field, final int value) {
        try {
            _generator.writeNumberField(field, value);
            return this;
        } catch (IOException e) {
            log.error("An error occurred", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public JsonGenerator write(final String field, final long value) {
        try {
            _generator.writeNumberField(field, value);
            return this;
        } catch (IOException e) {
            log.error("An error occurred", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public JsonGenerator write(final String field, final double value) {
        try {
            _generator.writeNumberField(field, value);
            return this;
        } catch (IOException e) {
            log.error("An error occurred", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public JsonGenerator write(final String field, final boolean value) {
        try {
            _generator.writeBooleanField(field, value);
            return this;
        } catch (IOException e) {
            log.error("An error occurred", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public JsonGenerator writeNull(final String field) {
        try {
            _generator.writeNullField(field);
            return this;
        } catch (IOException e) {
            log.error("An error occurred", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public JsonGenerator writeEnd() {
        try {
            _generator.writeEndObject();
            return this;
        } catch (IOException e) {
            log.error("An error occurred", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public JsonGenerator write(final JsonValue value) {
        try {
            _generator.writeRaw(value.toString());
            return this;
        } catch (IOException e) {
            log.error("An error occurred", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public JsonGenerator write(final String value) {
        try {
            _generator.writeString(value);
            return this;
        } catch (IOException e) {
            log.error("An error occurred", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public JsonGenerator write(final BigDecimal value) {
        try {
            _generator.writeNumber(value);
            return this;
        } catch (IOException e) {
            log.error("An error occurred", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public JsonGenerator write(final BigInteger value) {
        try {
            _generator.writeNumber(value);
            return this;
        } catch (IOException e) {
            log.error("An error occurred", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public JsonGenerator write(final int value) {
        try {
            _generator.writeNumber(value);
            return this;
        } catch (IOException e) {
            log.error("An error occurred", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public JsonGenerator write(final long value) {
        try {
            _generator.writeNumber(value);
            return this;
        } catch (IOException e) {
            log.error("An error occurred", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public JsonGenerator write(final double value) {
        try {
            _generator.writeNumber(value);
            return this;
        } catch (IOException e) {
            log.error("An error occurred", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public JsonGenerator write(final boolean value) {
        try {
            _generator.writeBoolean(value);
            return this;
        } catch (IOException e) {
            log.error("An error occurred", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public JsonGenerator writeNull() {
        try {
            _generator.writeNull();
            return this;
        } catch (IOException e) {
            log.error("An error occurred", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void close() {
        try {
            _generator.close();
        } catch (IOException e) {
            log.error("An error occurred", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void flush() {
        try {
            _generator.flush();
        } catch (IOException e) {
            log.error("An error occurred", e);
            throw new RuntimeException(e);
        }
    }

    private final com.fasterxml.jackson.core.JsonGenerator _generator;
}
