/*
 * dicom-store: org.nrg.xnat.dicom.workers.store.services.impl.jpa.JpaResourceRecordService
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.dicom.workers.store.services.impl.jpa;

import javax.transaction.Transactional;
import lombok.Getter;
import org.nrg.xnat.dicom.workers.store.data.ResourceRecord;
import org.nrg.xnat.dicom.workers.store.repositories.ResourceRecordRepository;
import org.nrg.xnat.dicom.workers.store.services.ResourceRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Getter
@Transactional
public class JpaResourceRecordService extends AbstractRecordService<ResourceRecord, ResourceRecordRepository> implements ResourceRecordService {
    @Autowired
    public JpaResourceRecordService(final ResourceRecordRepository repository) {
        this.repository = repository;
    }

    private final ResourceRecordRepository repository;
}
