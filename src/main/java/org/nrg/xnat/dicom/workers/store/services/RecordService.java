/*
 * dicom-store: org.nrg.xnat.dicom.workers.store.services.RecordService
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.dicom.workers.store.services;

import org.nrg.xnat.dicom.workers.store.data.BaseMetadata;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RecordService<E extends BaseMetadata, D extends JpaRepository<E, Long>> {
    D getRepository();

    E create(E entity);

    E retrieve(long id);

    E update(E entity);

    void delete(long id);

    void delete(E entity);
}
