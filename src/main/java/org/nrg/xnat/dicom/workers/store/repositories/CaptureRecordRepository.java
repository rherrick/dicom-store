/*
 * dicom-store: org.nrg.xnat.dicom.workers.store.repositories.CaptureRecordRepository
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.dicom.workers.store.repositories;

import org.nrg.xnat.dicom.workers.store.data.CaptureRecord;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CaptureRecordRepository extends JpaRepository<CaptureRecord, Long> {

}
