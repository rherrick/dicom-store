/*
 * dicom-store: org.nrg.xnat.dicom.workers.store.services.DicomAggregatorService
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.dicom.workers.store.services;

public interface DicomAggregatorService {
}
