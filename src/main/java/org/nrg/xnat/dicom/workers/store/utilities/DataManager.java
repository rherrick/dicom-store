/*
 * dicom-store: org.nrg.xnat.dicom.workers.store.utilities.DataManager
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.dicom.workers.store.utilities;

import java.io.File;
import java.nio.file.Path;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;

@Slf4j
public class DataManager {
    private DataManager() {
        // Just can't create one of these...
    }

    public static Path validateFolder(final Path folder) {
        final File file = folder.toFile();
        if (!file.exists()) {
            if (!file.mkdirs()) {
                throw new RuntimeException("Tried to create folder " + folder + " but that operation failed for some reason (no exception thrown).");
            }
            if (!file.canWrite()) {
                throw new RuntimeException("Created folder " + folder + " but apparently I can't write to it.");
            }
            log.info("The folder {} didn't exist, it's now created and writeable", folder);
        } else if (file.isFile()) {
            throw new RuntimeException("The path specified for the folder is " + folder + " but that already exists and is a file.");
        } else if (!file.canWrite()) {
            throw new RuntimeException("The folder at " + folder + " can't be written to by this process.");
        }
        return file.toPath();
    }

    public static String getFileSize(final File destinationFile) {
        return FileUtils.byteCountToDisplaySize(FileUtils.sizeOf(destinationFile));
    }
}
