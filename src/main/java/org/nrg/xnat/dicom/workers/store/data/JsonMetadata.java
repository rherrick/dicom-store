/*
 * dicom-store: org.nrg.xnat.dicom.workers.store.data.JsonMetadata
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.dicom.workers.store.data;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import java.util.Date;

@EntityListeners(AuditingEntityListener.class)
@MappedSuperclass
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public abstract class JsonMetadata extends BaseMetadata {
    public JsonMetadata(final Long id, final String createdBy, final Date created, final String lastModifiedBy, final Date lastModified, final String format, final JsonNode processor, final JsonNode metadata) {
        super(id, createdBy, created, lastModifiedBy, lastModified);
        setFormat(format);
        setProcessor(processor);
        setMetadata(metadata);
    }

    private String format;

    @Type(type = "JsonB")
    @Column(columnDefinition = "JsonB")
    private JsonNode processor;

    @Type(type = "JsonB")
    @Column(columnDefinition = "JsonB")
    private JsonNode metadata;
}
