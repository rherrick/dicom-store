/*
 * dicom-store: org.nrg.xnat.dicom.workers.store.services.CaptureRecordService
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.dicom.workers.store.services;

import org.nrg.xnat.dicom.workers.store.data.CaptureRecord;
import org.nrg.xnat.dicom.workers.store.repositories.CaptureRecordRepository;

public interface CaptureRecordService extends RecordService<CaptureRecord, CaptureRecordRepository> {
    String CAPTURE = "capture";
}
