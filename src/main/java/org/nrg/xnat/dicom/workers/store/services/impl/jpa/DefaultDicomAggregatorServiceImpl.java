/*
 * dicom-store: org.nrg.xnat.dicom.workers.store.services.impl.jpa.DefaultDicomAggregatorServiceImpl
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.dicom.workers.store.services.impl.jpa;

import static org.nrg.xnat.dicom.workers.store.services.ResourceEntityService.AGGREGATE;
import static org.nrg.xnat.dicom.workers.store.services.ResourceRecordService.EXTRACT;

import lombok.extern.slf4j.Slf4j;
import org.nrg.xnat.dicom.workers.store.LaunchCommand;
import org.nrg.xnat.dicom.workers.store.data.ResourceRecord;
import org.nrg.xnat.dicom.workers.store.services.DicomAggregatorService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Profile(AGGREGATE)
@Service
@Slf4j
public class DefaultDicomAggregatorServiceImpl implements DicomAggregatorService {
    @Autowired
    public DefaultDicomAggregatorServiceImpl(final LaunchCommand command) {
        command.call();
    }

    @RabbitListener(queues = EXTRACT)
    public void listen(final ResourceRecord resourceRecord) {
        log.info("Just got a record from the queue {}: {}", EXTRACT, resourceRecord);
    }
}
