/*
 * dicom-store: org.nrg.xnat.dicom.workers.store.services.ResourceRecordService
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.dicom.workers.store.services;

import org.nrg.xnat.dicom.workers.store.data.ResourceRecord;
import org.nrg.xnat.dicom.workers.store.repositories.ResourceRecordRepository;

public interface ResourceRecordService extends RecordService<ResourceRecord, ResourceRecordRepository> {
    String EXTRACT = "extract";
}
