/*
 * dicom-store: org.nrg.xnat.dicom.workers.store.data.ResourceRecord
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.dicom.workers.store.data;

import static javax.persistence.TemporalType.TIMESTAMP;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Temporal;
import java.util.Date;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ResourceRecord extends JsonMetadata {
    @Builder
    public ResourceRecord(final Long id, final String createdBy, final Date created, final String lastModifiedBy, final Date lastModified, final String format, final JsonNode processor, final JsonNode metadata, final String location, final Date extractionStartTime, final Date extractionEndTime, final String referenceType, final long referenceId) {
        super(id, createdBy, created, lastModifiedBy, lastModified, format, processor, metadata);
        setLocation(location);
        setExtractionStartTime(extractionStartTime);
        setExtractionEndTime(extractionEndTime);
        setReferenceType(referenceType);
        setReferenceId(referenceId);
    }

    @NonNull
    @Column(unique = true)
    private String location;

    @Temporal(TIMESTAMP)
    private Date extractionStartTime;

    @Temporal(TIMESTAMP)
    private Date extractionEndTime;

    private String referenceType;

    private long referenceId;
}
