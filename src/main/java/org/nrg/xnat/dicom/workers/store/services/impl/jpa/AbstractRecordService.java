/*
 * dicom-store: org.nrg.xnat.dicom.workers.store.services.impl.jpa.AbstractRecordService
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.dicom.workers.store.services.impl.jpa;

import javax.transaction.Transactional;
import org.nrg.xnat.dicom.workers.store.data.BaseMetadata;
import org.nrg.xnat.dicom.workers.store.services.RecordService;
import org.springframework.data.jpa.repository.JpaRepository;

@Transactional
public abstract class AbstractRecordService<E extends BaseMetadata, D extends JpaRepository<E, Long>> implements RecordService<E, D> {

    @Override
    public E create(final E entity) {
        return update(entity);
    }

    @Override
    public E retrieve(final long id) {
        return getRepository().getOne(id);
    }

    @Override
    public E update(final E entity) {
        return getRepository().saveAndFlush(entity);
    }

    @Override
    public void delete(final long id) {
        getRepository().deleteById(id);
    }

    @Override
    public void delete(final E entity) {
        getRepository().delete(entity);
    }
}
