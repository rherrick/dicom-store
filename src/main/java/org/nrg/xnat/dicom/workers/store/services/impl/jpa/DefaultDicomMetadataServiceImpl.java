/*
 * dicom-store: org.nrg.xnat.dicom.workers.store.services.impl.jpa.DefaultDicomMetadataServiceImpl
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.dicom.workers.store.services.impl.jpa;

import static org.nrg.xnat.dicom.workers.store.services.CaptureRecordService.CAPTURE;
import static org.nrg.xnat.dicom.workers.store.services.ResourceRecordService.EXTRACT;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.StopWatch;
import org.dcm4che3.data.Attributes;
import org.dcm4che3.data.Tag;
import org.dcm4che3.io.DicomInputStream;
import org.dcm4che3.json.JSONWriter;
import org.dcm4che3.util.StringUtils;
import org.nrg.xnat.dicom.workers.store.LaunchCommand;
import org.nrg.xnat.dicom.workers.store.data.CaptureRecord;
import org.nrg.xnat.dicom.workers.store.data.ResourceRecord;
import org.nrg.xnat.dicom.workers.store.services.DicomMetadataService;
import org.nrg.xnat.dicom.workers.store.services.ResourceRecordService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.converter.json.Jackson2ObjectMapperFactoryBean;
import org.springframework.stereotype.Service;

import javax.json.Json;
import javax.json.stream.JsonGenerator;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Date;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

@Profile(EXTRACT)
@Service
@Getter(AccessLevel.PROTECTED)
@Accessors(prefix = "_")
@Slf4j
public class DefaultDicomMetadataServiceImpl implements DicomMetadataService {
    @Autowired
    public DefaultDicomMetadataServiceImpl(final ResourceRecordService resourceRecordService, final RabbitTemplate rabbitTemplate, final Jackson2ObjectMapperFactoryBean objectMapperBuilderFactory, final LaunchCommand command) {
        _resourceRecordService = resourceRecordService;
        _rabbitTemplate = rabbitTemplate;
        _objectMapper = Objects.requireNonNull(objectMapperBuilderFactory.getObject());
        _processor = _objectMapper.createObjectNode();
        _processor.put("implementation", getClass().getName());
        command.call();
    }

    @RabbitListener(queues = CAPTURE)
    public void listen(final CaptureRecord captureRecord) {
        log.info("Just got a record from the queue {}: {}", CAPTURE, captureRecord);
        if (!StringUtils.equals("DICOM", captureRecord.getFormat())) {
            // TODO: Re-queue the message, this guy doesn't want to deal with it.
            log.warn("I'm only supposed to handle DICOM, but I found a non-DICOM data format: {}", captureRecord.getFormat());
        }

        final ResourceRecord resourceRecord = extract(captureRecord);

        // TODO: Explicit references to RabbitMQ should be replaced by message service that can send to the appropriate destination.
        _rabbitTemplate.convertAndSend(EXTRACT, resourceRecord);
    }

    @Override
    public ResourceRecord extract(final CaptureRecord captureRecord) {
        final StopWatch stopWatch = StopWatch.createStarted();

        // Write out DICOM metadata as JSON
        final StringWriter    writer   = new StringWriter(1024 * 1024);
        @NonNull final String location = captureRecord.getLocation();
        try (final InputStream input = new URI(location).toURL().openStream();
             final DicomInputStream inputStream = new DicomInputStream(input);
             final JsonGenerator generator = Json.createGenerator(writer)) {
            final Attributes attributes = inputStream.readDataset(-1, Tag.PixelData);
            attributes.removeSelected(Arrays.stream(attributes.tags()).filter(tag -> attributes.getVR(tag).isInlineBinary()).toArray());
            final JSONWriter jsonWriter = new JSONWriter(generator);
            jsonWriter.write(attributes);
        } catch (IOException | URISyntaxException e) {
            throw new RuntimeException(e);
        }

        final String dicom = writer.toString();
        stopWatch.stop();
        log.info("Completed extracting {} bytes of DICOM from file {} in {} ms", dicom.length(), location, stopWatch.getTime(TimeUnit.MILLISECONDS));

        try {
            return _resourceRecordService.create(ResourceRecord.builder()
                                                               .format("DICOM")
                                                               .location(location)
                                                               .processor(_processor)
                                                               .metadata(_objectMapper.readTree(dicom))
                                                               .extractionStartTime(new Date(stopWatch.getStartTime()))
                                                               .extractionEndTime(new Date())
                                                               .build());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    private final ResourceRecordService _resourceRecordService;
    private final RabbitTemplate        _rabbitTemplate;
    private final ObjectMapper          _objectMapper;
    private final ObjectNode            _processor;
}
