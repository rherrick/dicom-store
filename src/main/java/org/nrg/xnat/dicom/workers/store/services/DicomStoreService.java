/*
 * dicom-store: org.nrg.xnat.dicom.workers.store.services.DicomStoreService
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.dicom.workers.store.services;

import org.dcm4che3.data.Attributes;
import org.dcm4che3.net.Association;
import org.dcm4che3.net.PDVInputStream;
import org.dcm4che3.net.pdu.PresentationContext;
import org.dcm4che3.net.service.DicomServiceException;
import org.nrg.xnat.dicom.workers.store.data.CaptureRecord;

public interface DicomStoreService {
    CaptureRecord capture(final Association incoming, final PresentationContext context, final Attributes request, final PDVInputStream data, final Attributes response) throws DicomServiceException;

    int getStatus();

    @SuppressWarnings("unused")
    String getAeTitle();

    @SuppressWarnings("unused")
    int getPort();
}
