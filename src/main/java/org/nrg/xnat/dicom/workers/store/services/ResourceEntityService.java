/*
 * dicom-store: org.nrg.xnat.dicom.workers.store.services.ResourceEntityService
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.dicom.workers.store.services;

import org.nrg.xnat.dicom.workers.store.data.ResourceRecord;

/**
 * Handles storing resource entities, aggregated collections of atomic {@link ResourceRecord resource record entries}.
 */
public interface ResourceEntityService {
    String AGGREGATE = "aggregate";
}
