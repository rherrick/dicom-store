/*
 * dicom-store: org.nrg.xnat.dicom.workers.store.DicomStoreServiceApplicationTests
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.dicom.workers.store;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class DicomStoreServiceApplicationTests {
    @Test
    void contextLoads() {
    }
}
