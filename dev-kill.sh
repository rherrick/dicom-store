#!/usr/bin/env bash

killByPid() {
    PROFILES=(capture extract aggregate)
    for PROFILE in ${PROFILES[@]}; do
        PID_FILE="./store/${PROFILE}.pid"
        [[ -f ${PID_FILE} ]] && { kill $(cat ${PID_FILE}); rm -f ${PID_FILE}; } || { echo "I didn't find the PID file ${PID_FILE}. This may be okay, but I can't kill that process without the PID."; }
    done
}

shutdown() {
    for PORT in 8080 8081 8082; do
        curl --user xnat:xnat --request POST http://localhost:${PORT}/actuator/shutdown
    done
    rm -f ./store/*.pid
}

shutdown

