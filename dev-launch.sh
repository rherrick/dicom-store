#!/usr/bin/env bash

PROFILES=(capture extract aggregate)

checkForPidFiles() {
    [[ ! -d ./store ]] && { mkdir ./store; return; }
    for PROFILE in ${PROFILES[@]}; do
        [[ -f ./store/${PROFILE}.pid ]] && { echo "Found PID file ./store/${PROFILE}.pid. You should kill that process if it's still alive or remove the PID file."; exit 1; }
    done
}

buildApp() {
    local STATUS=0
    [[ ! -f build/libs/dicom-store-1.0-SNAPSHOT-boot.jar ]] && { ./gradlew bootJar; STATUS=${?}; }
    [[ ${STATUS} != 0 ]] && { echo "Something went wrong with the bootJar build. Check the build output and try again."; }
}

launchProcess() {
    local PROFILE=${1}
    local OPTIONS="./store"
    local DEBUG_PORT=8000

    case "$${PROFILE}" in
        capture)
            OPTIONS="--ae-title=XNAT --port=8104 ${OPTIONS}";
            ;;
         
        extract)
            DEBUG_PORT=8001
            ;;

         aggregate)
            DEBUG_PORT=8002
            ;;
    esac
    
    java -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=${DEBUG_PORT} -jar -Dspring.profiles.active=dev,${PROFILE} build/libs/dicom-store-1.0-SNAPSHOT-boot.jar --mode=${PROFILE} ${OPTIONS} &> ./store/${PROFILE}.log &
    echo ${!} > ./store/${PROFILE}.pid
    echo "Started service for ${PROFILE} with PID $(cat ./store/${PROFILE}.pid) and console output in ./store/${PROFILE}.log. You can debug this service by connecting to port ${DEBUG_PORT}."
}

countStarted() {
    echo "$(grep -E "Started Dicom[A-z]+ in" ./store/*.log | wc -l | tr -d ' ')"
}

main() {
    checkForPidFiles
    buildApp
    for PROFILE in ${PROFILES[@]}; do
        launchProcess ${PROFILE}
    done
    local COMPLETED=$(countStarted)
    while [ ${COMPLETED} != ${#PROFILES[@]} ]; do
        echo "${COMPLETED} services have completed start-up, waiting..."
        sleep 5
        COMPLETED=$(countStarted)
    done
    echo
    echo "$(for PROFILE in ${PROFILES[@]}; do echo "$(echo ${PROFILE^}):"; echo "$(cat ./store/${PROFILE}.log)"; echo; echo; done)"
}

main
